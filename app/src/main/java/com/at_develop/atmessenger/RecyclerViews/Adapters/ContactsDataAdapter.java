package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.at_develop.atmessenger.ChatMessage;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.R;
import com.bumptech.glide.Glide;
import com.cloudsourceit.cloudtools.recyclerview.CursorRecyclerAdapter;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by jgcapistran on 01/11/17.
 *
 */

public class ContactsDataAdapter extends CursorRecyclerAdapter<ContactsDataAdapter.ViewHolder> {

    /**
     * Recommended constructor.
     *
     * @param context The context
     * @param c       The cursor from which to get the data.
     * @param flags   Flags used to determine the behavior of the adapter; may
     *                be any combination of {@link #FLAG_AUTO_REQUERY} and
     *                {@link #FLAG_REGISTER_CONTENT_OBSERVER}.
     */

    public ContactsDataAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }


    @Override
    public ContactsDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_layout, parent, false);

        return new ContactsDataAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void bindViewHolder(final ContactsDataAdapter.ViewHolder holder, Cursor cursor) {

        holder.personUserName.setText(cursor.getString(4));
        holder.personEstado.setText(cursor.getString(9));
        holder.id_friend = cursor.getInt(1);
        holder.statusUser = cursor.getInt(8);
        holder.profileImg = cursor.getString(6);

        switch (cursor.getInt(8)) {
            case 0:
                holder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{holder.context.getResources().getColor(R.color.desconectado),holder.context.getResources().getColor(R.color.desconectado)}
                ));
                holder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 1:
                holder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{holder.context.getResources().getColor(R.color.disponible),holder.context.getResources().getColor(R.color.disponible)}
                ));
                holder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 2:
                holder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{holder.context.getResources().getColor(R.color.nodisponible),holder.context.getResources().getColor(R.color.nodisponible)}
                ));
                holder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 3:
                holder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{holder.context.getResources().getColor(R.color.ausente),holder.context.getResources().getColor(R.color.ausente)}
                ));
                holder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
        }

        Glide.with(holder.personPhoto).load(cursor.getString(6))
                .apply(bitmapTransform(new CropCircleTransformation()))
                .into(holder.personPhoto);

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String token_user = "";
                User_Information_Helper create_database_user_information =
                        new User_Information_Helper(v.getContext());
                SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

                // Nos aseguramos de que existe al menos un registro
                if (result_query.moveToFirst()) {
                    // Obtencion de los datos de usuario desde la base de datos
                    token_user = result_query.getString(13);
                }
                db.close();

                Intent chatMessage = new Intent(holder.context.getApplicationContext(), ChatMessage.class);
                chatMessage.putExtra("friend_user_name", holder.personUserName.getText());
                chatMessage.putExtra("friend_id", holder.id_friend);
                chatMessage.putExtra("friend_estado_user", holder.personEstado.getText());
                chatMessage.putExtra("friend_status_user", holder.statusUser);
                chatMessage.putExtra("friend_img_profile", holder.profileImg);
                chatMessage.putExtra("token_user", token_user);

                holder.context.startActivity(chatMessage);
            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final TextView personUserName;
        final TextView personEstado;
        final ImageView personPhoto;
        final LinearLayout personStatus;
        final Context context;
        final View item;
        int id_friend;
        int statusUser;
        String profileImg;

        public ViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            personUserName = itemView.findViewById(R.id.person_name);
            personEstado = itemView.findViewById(R.id.person_age);
            personStatus = itemView.findViewById(R.id.container_person_photo);
            personPhoto = itemView.findViewById(R.id.person_photo);
            context = itemView.getContext();
            item = itemView;
            id_friend = 0;
            statusUser = 1;
            profileImg = "";
        }
    }
}