package com.at_develop.atmessenger.RecyclerViews.Adapters;

import com.at_develop.atmessenger.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cloudsourceit on 14/10/17.
 */

public class Friends {
    int id;
    String user_name;
    String profile_img;
    int status_user;
    String estado_user;

    public Friends(int id, String user_name, String profile_img, int status_user, String estado_user) {
        this.id = id;
        this.user_name = user_name;
        this.profile_img = profile_img;
        this.status_user = status_user;
        this.estado_user = estado_user;
    }
}