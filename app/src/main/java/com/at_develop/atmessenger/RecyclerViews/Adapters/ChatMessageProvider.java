package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;

/**
 * Created by jgcapistran on 25/10/17.
 */

public class ChatMessageProvider extends ContentProvider {
    private static final String INVALID_URI_MESSAGE = "Invalid Uri: ";
    private static final String EQUALS = "=";
    public static final String AUTHORITY_PART = "com.at_develop.atmessenger";
    public static final int CODE_ALL_ITEMS = 1;
    public static final int CODE_SINGLE_ITEM = 2;
    public static final String CONTENT_PREFIX = "content://";
    public static final Uri CONTENT_URI = Uri.parse(CONTENT_PREFIX + AUTHORITY_PART + "/messages");
    public static final String MIME_TYPE_ALL_ITEMS = "vnd.android.cursor.dir/vnd.org.examples.android";
    public static final String MIME_TYPE_SINGLE_ITEM = "vnd.android.cursor.item/vnd.org.examples.android";
    public static final UriMatcher URI_MATCHER;
    private SQLiteDatabase database;
    private User_Information_Helper dbHelper;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY_PART, "messages", CODE_ALL_ITEMS);
        URI_MATCHER.addURI(AUTHORITY_PART, "messages/#", CODE_SINGLE_ITEM);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        int rowsAffected = 0;
        switch (URI_MATCHER.match(uri)) {
            case CODE_ALL_ITEMS:
                rowsAffected = this.getOrOpenDatabase().delete(User_Messages_Schema.User_Messages_Database.TABLE_NAME, where, whereArgs);
                break;
            case CODE_SINGLE_ITEM:
                String singleRecordId = uri.getPathSegments().get(1);
                rowsAffected = this.getOrOpenDatabase().delete(User_Messages_Schema.User_Messages_Database.TABLE_NAME, User_Messages_Schema.User_Messages_Database._ID + EQUALS + singleRecordId, whereArgs);
                break;
            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE + uri);
        }
        return rowsAffected;
    }

    /**
     * @return
     */
    private SQLiteDatabase getOrOpenDatabase() {
        SQLiteDatabase db = null;
        if (this.database != null && database.isOpen()) {
            db = this.database;
        } else {
            db = dbHelper.getWritableDatabase();
        }
        return db;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case CODE_ALL_ITEMS:
                return MIME_TYPE_ALL_ITEMS;
            case CODE_SINGLE_ITEM:
                return MIME_TYPE_SINGLE_ITEM;
            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE + uri);

        }
    }
    @Override
    public Uri insert(Uri arg0, ContentValues arg1) {
        long rowID = getOrOpenDatabase().insert(User_Messages_Schema.User_Messages_Database.TABLE_NAME,User_Information_Helper.DATABASE_NAME,arg1);

        Uri newRecordUri = null;
        switch(URI_MATCHER.match(arg0)){
            case CODE_ALL_ITEMS:
                if (rowID > 0){
                    newRecordUri = ContentUris.withAppendedId(CONTENT_URI,rowID);
                }
                break;
            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE + arg0);
        }
        return newRecordUri;
    }
    @Override
    public boolean onCreate() {
        dbHelper = User_Information_Helper.getInstance(getContext());
        database = dbHelper.getWritableDatabase();
        return database != null && database.isOpen();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
/* The database is closed if more memory is needed */
        this.dbHelper.close();
    }
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[]
            selectionArgs,String sort) {
        Cursor cursor = null;
        SQLiteQueryBuilder qBuilder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case CODE_SINGLE_ITEM:
                String id = uri.getPathSegments().get(1);
                qBuilder.appendWhere(User_Messages_Schema.User_Messages_Database._ID + EQUALS + id);
                break;
            case UriMatcher.NO_MATCH:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE+uri);

        }
        qBuilder.setTables(User_Messages_Schema.User_Messages_Database.TABLE_NAME);
        cursor = qBuilder.query(getOrOpenDatabase(),projection,selection,selectionArgs,null,null,null);
        return cursor;
    }
    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int rowsAffected = 0;
        String recordId = null;
        switch (URI_MATCHER.match(uri)) {
            case CODE_ALL_ITEMS:
                rowsAffected = getOrOpenDatabase().update(User_Messages_Schema.User_Messages_Database.TABLE_NAME, values, where, whereArgs);
                break;
            case CODE_SINGLE_ITEM:
                recordId = uri.getPathSegments().get(1);
                rowsAffected = getOrOpenDatabase().update(User_Messages_Schema.User_Messages_Database.TABLE_NAME, values, User_Messages_Schema.User_Messages_Database._ID + EQUALS + recordId, whereArgs);

                break;
            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE+uri);

        }
        return rowsAffected;
    }
}

