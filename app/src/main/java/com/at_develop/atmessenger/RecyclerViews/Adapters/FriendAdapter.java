package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.at_develop.atmessenger.ChatMessage;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.R;
import com.bumptech.glide.Glide;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by jgcapistran on 14/10/17.
 */

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.FriendViewHolder>{

    List<Friends> persons;

    public FriendAdapter(List<Friends> persons){
        this.persons = persons;
    }


    public static class FriendViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final TextView personUserName;
        final TextView personEstado;
        final ImageView personPhoto;
        final LinearLayout personStatus;
        final Context context;
        final View item;
        int id_friend;
        int statusUser;
        String profileImg;

        public FriendViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            personUserName = (TextView)itemView.findViewById(R.id.person_name);
            personEstado = (TextView)itemView.findViewById(R.id.person_age);
            personStatus = (LinearLayout) itemView.findViewById(R.id.container_person_photo);
            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
            context = itemView.getContext();
            item = itemView;
            id_friend = 0;
            statusUser = 1;
            profileImg = "";
        }
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_layout, viewGroup, false);
        FriendViewHolder pvh = new FriendViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final FriendViewHolder personViewHolder, int i) {
        personViewHolder.personUserName.setText(persons.get(i).user_name);
        personViewHolder.personEstado.setText(persons.get(i).estado_user);
        personViewHolder.id_friend = persons.get(i).id;
        personViewHolder.statusUser = persons.get(i).status_user;
        personViewHolder.profileImg = persons.get(i).profile_img;

        switch (persons.get(i).status_user) {
            case 0:
                personViewHolder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{personViewHolder.context.getResources().getColor(R.color.desconectado),personViewHolder.context.getResources().getColor(R.color.desconectado)}
                ));
                personViewHolder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 1:
                personViewHolder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{personViewHolder.context.getResources().getColor(R.color.disponible),personViewHolder.context.getResources().getColor(R.color.disponible)}
                ));
                personViewHolder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 2:
                personViewHolder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{personViewHolder.context.getResources().getColor(R.color.nodisponible),personViewHolder.context.getResources().getColor(R.color.nodisponible)}
                ));
                personViewHolder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 3:
                personViewHolder.personStatus.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{personViewHolder.context.getResources().getColor(R.color.ausente),personViewHolder.context.getResources().getColor(R.color.ausente)}
                ));
                personViewHolder.personStatus.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
        }

        Glide.with(personViewHolder.personPhoto).load(persons.get(i).profile_img)
                .apply(bitmapTransform(new CropCircleTransformation()))
                .into(personViewHolder.personPhoto);

        personViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String token_user = "";
                User_Information_Helper create_database_user_information =
                        new User_Information_Helper(v.getContext());
                SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

                // Nos aseguramos de que existe al menos un registro
                if (result_query.moveToFirst()) {
                    // Obtencion de los datos de usuario desde la base de datos
                    token_user = result_query.getString(13);
                }
                db.close();

                Intent chatMessage = new Intent(personViewHolder.context.getApplicationContext(), ChatMessage.class);
                chatMessage.putExtra("friend_user_name", personViewHolder.personUserName.getText());
                chatMessage.putExtra("friend_id", personViewHolder.id_friend);
                chatMessage.putExtra("friend_estado_user", personViewHolder.personEstado.getText());
                chatMessage.putExtra("friend_status_user", personViewHolder.statusUser);
                chatMessage.putExtra("friend_img_profile", personViewHolder.profileImg);
                chatMessage.putExtra("token_user", token_user);

                personViewHolder.context.startActivity(chatMessage);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}