package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Friends_Schema;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatMessageContract.*;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ContactsContract.*;
import java.util.ArrayList;

/**
 * Created by cloudsourceit on 26/10/17.
 */

public class ChatDataProvider extends ContentProvider{

    //public static final String CONTENT_AUTHORITY = "com.at_develop.atmessenger.data.messages";
    public static final String CONTENT_AUTHORITY = "com.at_develop.atmessenger";
    private static final String INVALID_URI_MESSAGE = "Invalid Uri: ";
    private static final String EQUALS = " = ";
    public static final int CODE_ALL_MESSAGES = 100;
    public static final int CODE_ALL_CONTACTS = 110;
    public static final int CODE_ALL_SESSION = 200; // all items non filtered
    public static final int CODE_LIST_CHAT = 120;
    public static final int CODE_SINGLE_SESSION = 210; // all items non filtered

    public static final int CODE_ALL_ITEMS = 1;
    public static final int CODE_SINGLE_ITEM = 2;
    public static final String CONTENT_PREFIX = "content://";
    public static final Uri CONTENT_URI = Uri.parse(CONTENT_PREFIX + CONTENT_AUTHORITY + "/messages");
    public static final Uri CONTENT_URI_C = Uri.parse(CONTENT_PREFIX + CONTENT_AUTHORITY + "/contacts");
    public static final Uri CONTENT_URI_M = Uri.parse(CONTENT_PREFIX + CONTENT_AUTHORITY + "/message_contacts");

    public static final int CODE_ALL_CHECKIN_CHECKOUT = 300; // all items non filtered
    public static final int CODE_SINGLE_CHECKIN_CHECKOUT = 310; // single item


    private User_Information_Helper mMessagesDatabase;
    public static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(CONTENT_AUTHORITY, ChatMessageContract.PATH_MESSAGE, CODE_ALL_MESSAGES);
        URI_MATCHER.addURI(CONTENT_AUTHORITY, ContactsContract.PATH_CONTACT, CODE_ALL_CONTACTS);
        URI_MATCHER.addURI(CONTENT_AUTHORITY, ContactsContract.PATH_CONTACT_M, CODE_LIST_CHAT);
        URI_MATCHER.addURI(CONTENT_AUTHORITY, ChatMessageContract.PATH_MESSAGE + "/#", CODE_SINGLE_SESSION);

    }

    @Override
    public boolean onCreate() {
        mMessagesDatabase = User_Information_Helper.getInstance(getContext());
        return true;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMessagesDatabase.close();
    }

    @Override
    public String getType(@NonNull Uri uri) {
        int uriType = URI_MATCHER.match(uri);
        switch (uriType) {
            case CODE_ALL_MESSAGES:
                return Message.CONTENT_TYPE;
            case CODE_ALL_CONTACTS:
                return Contacts.CONTENT_TYPE;
            case CODE_ALL_SESSION:
                return Message.CONTENT_TYPE;
            case CODE_SINGLE_SESSION:
                return Message.CONTENT_ITEM_TYPE;
            case CODE_ALL_CHECKIN_CHECKOUT:
                return Message.CONTENT_TYPE;
            case CODE_LIST_CHAT:
                return Contacts.CONTENT_TYPE_M;
            default:
                return null;
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Uri newUri;
        long newId;
        final int match = URI_MATCHER.match(uri);
        final SQLiteDatabase db = mMessagesDatabase.getWritableDatabase();
        switch (match) {
            case CODE_ALL_MESSAGES:
                newId = db.insert(User_Messages_Schema.User_Messages_Database.TABLE_NAME, null, values);
                newUri = Message.buildItemUri(newId);
                break;
            case CODE_ALL_CONTACTS:
                newId = db.insert(User_Friends_Schema.User_Friends_Database.TABLE_NAME, null, values);
                newUri = Message.buildItemUri(newId);
                break;
            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE + uri + " for insert ");
        }
        try{
        if (newId == -1) {
            throw new SQLException("Failed to insert row into " + uri);
        }

            getContext().getContentResolver().notifyChange(newUri, null);
        }catch (Exception e){
            Log.wtf("dddd", e.toString());
        }
        return newUri;
    }


    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final String id;
        final Cursor cursor;
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String groupBy = null;
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case CODE_ALL_MESSAGES:
                qb.setTables(User_Messages_Schema.User_Messages_Database.TABLE_NAME);
                //qb.appendWhere(Session.KEY_SESSION_ACTIVE + " = 1");
                break;
            case CODE_ALL_CONTACTS:
                qb.setTables(User_Friends_Schema.User_Friends_Database.TABLE_NAME);
                //qb.appendWhere(Session.KEY_SESSION_ACTIVE + " = 1");
                break;
            case CODE_SINGLE_SESSION:
                qb.setTables(User_Messages_Schema.User_Messages_Database.TABLE_NAME);
                id = uri.getLastPathSegment();
                //qb.appendWhere(Session.KEY_SESSION_ID + EQUALS + id+ " AND " + Session.KEY_SESSION_ACTIVE + " = 1");
                break;
            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE+uri);

        }

        final SQLiteDatabase db = mMessagesDatabase.getReadableDatabase();
        cursor = qb.query(db, projection, selection, selectionArgs, groupBy, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }


    /**
     * Apply the given set of {@link ContentProviderOperation}, executing inside
     * a {@link SQLiteDatabase} transaction. All changes will be rolled back if
     * any single one fails.
     */
    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = mMessagesDatabase.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int rowsDeleted=0;
        String deleteId;
        final int match=URI_MATCHER.match(uri);
        final SQLiteDatabase db = mMessagesDatabase.getWritableDatabase();
        switch (match) {
            case CODE_ALL_MESSAGES:
                db.beginTransaction();
                try {
                    rowsDeleted = db.delete(User_Messages_Schema.User_Messages_Database.TABLE_NAME, selection, selectionArgs);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case CODE_ALL_CONTACTS:
                db.beginTransaction();
                try {
                    rowsDeleted = db.delete(User_Friends_Schema.User_Friends_Database.TABLE_NAME, selection, selectionArgs);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;
            case CODE_ALL_CHECKIN_CHECKOUT:
                db.beginTransaction();
                try {
                    rowsDeleted = db.delete(User_Messages_Schema.User_Messages_Database.TABLE_NAME, selection, selectionArgs);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                break;

            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE+uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int rowsAffected;
        String recordId;
        final int match = URI_MATCHER.match(uri);
        final SQLiteDatabase db = mMessagesDatabase.getWritableDatabase();
        switch (match) {
            case CODE_SINGLE_SESSION:
                recordId=Message.getItemId(uri);
                //selection=N.KEY_SESSION_ID + EQUALS +recordId;
                rowsAffected=db.update(User_Messages_Schema.User_Messages_Database.TABLE_NAME, values, selection, selectionArgs);
                break;

            default:
                throw new IllegalArgumentException(INVALID_URI_MESSAGE+uri);

        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }
}