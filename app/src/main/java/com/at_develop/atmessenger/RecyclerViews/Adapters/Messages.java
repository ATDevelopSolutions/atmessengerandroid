package com.at_develop.atmessenger.RecyclerViews.Adapters;

import java.security.Timestamp;

/**
 * Created by jgcapistran on 15/10/17.
 */

public class Messages {
    int id;
    String message;
    String createdAt;
    int to_me;

    public Messages(int id, String message, String createdAt, int to_me) {
        this.id = id;
        this.message = message;
        this.createdAt = createdAt;
        this.to_me = to_me;

    }
}
