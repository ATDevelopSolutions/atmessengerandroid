package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.database.Cursor;

import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;

/**
 * Created by jgcapistran on 23/10/17.
 */

public class MessageList {
    private String name;
    private String send_msj;

    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setSend_msj(String send_msj){
        this.send_msj=send_msj;
    }
    public String getSend_msj(){
        return send_msj;
    }

    public static MessageList fromCursor(Cursor cursor) {
        MessageList listItem = new MessageList();
        listItem.setName(cursor.getString(cursor.getColumnIndex(User_Messages_Schema.User_Messages_Database.MESSSAGE)));
        listItem.setSend_msj(cursor.getString(cursor.getColumnIndex(User_Messages_Schema.User_Messages_Database.MESSAGE_SEND)));
        return listItem;
    }
}