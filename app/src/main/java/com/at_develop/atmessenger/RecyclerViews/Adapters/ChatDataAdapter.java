package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.R;
import com.cloudsourceit.cloudtools.recyclerview.CursorRecyclerAdapter;

/**
 * Created by cloudsourceit on 26/10/17.
 */

public class ChatDataAdapter extends CursorRecyclerAdapter<ChatDataAdapter.ViewHolder> {

    /**
     * Recommended constructor.
     *
     * @param context The context
     * @param c       The cursor from which to get the data.
     * @param flags   Flags used to determine the behavior of the adapter; may
     *                be any combination of {@link #FLAG_AUTO_REQUERY} and
     *                {@link #FLAG_REGISTER_CONTENT_OBSERVER}.
     */

    public ChatDataAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;

        User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(getContext());
        SQLiteDatabase db = create_database_user_information.getWritableDatabase();
        Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);
        int id = 0;
        if (result_query.moveToFirst()) {
            id = result_query.getInt(1);
        }

        db.close();

        if (getCursor().getInt(3) == id) {
            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sended_messages, parent, false);
        }else{
            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.received_messages, parent, false);
        }

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void bindViewHolder(ViewHolder holder, Cursor cursor) {

        HelperWS parsed = new HelperWS();
        final String message = cursor.getString(2);
        final String send_message = parsed.parseHourTimestampWZone(cursor.getString(8));

        holder.message_tv.setText(message);
        holder.send_message_tv.setText(send_message);

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView message_tv;
        final TextView send_message_tv;

        ViewHolder(View itemView) {
            super(itemView);
            message_tv = (TextView) itemView.findViewById(R.id.text_message_body);
            send_message_tv = (TextView) itemView.findViewById(R.id.text_message_time);
        }

    }
}