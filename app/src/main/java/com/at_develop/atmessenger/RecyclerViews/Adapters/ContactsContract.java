package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import com.at_develop.atmessenger.Database.Schema.User_Friends_Schema;

/**
 * Created by jgcapistran on 01/11/17.
 */

public class ContactsContract {

    static final String PATH_CONTACT = "contacts";
    static final String PATH_CONTACT_M = "message_contacts";
    static final String PATH_EVENT = "event";
    static final String PATH_CHECKIN_CHECKOUT = "checkin_checkout";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + ChatDataProvider.CONTENT_AUTHORITY);

    private interface ContactsColumns {
        String ID = BaseColumns._ID;
        String FIRST_NAME = "first_name";
        String LAST_NAME = "last_name";
        String USER_NAME = "username";
        String EMAIL = "email";
        String PROFILE_IMG = "profile_img";
        String STATUS = "status";
        String STATUS_USER = "status_user";
        String ESTADO_USER = "estado_user";
        String SEX = "sex";
        String PAIS = "pais";
        String PHONE = "phone";
    }


    public static class Contacts extends ContactsContract.Base implements ContactsContract.ContactsColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONTACT).build();
        static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + PATH_CONTACT;
        static final String CONTENT_TYPE_M = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + PATH_CONTACT_M;
        static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + PATH_CONTACT;
        public static final String DEFAULT_SORT_ORDER = User_Friends_Schema.User_Friends_Database.TABLE_NAME + "." + ContactsColumns.USER_NAME + " ASC";

        static Uri buildItemUri(long itemId) {
            return ContentUris.withAppendedId(CONTENT_URI, itemId);
        }
    }

    static abstract class Base {
        static String getItemId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }
}