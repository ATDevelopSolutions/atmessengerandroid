package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.at_develop.atmessenger.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jgcapistran on 15/10/17.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 0;

    List<Messages> messages = new ArrayList<>();
    Context mContext = null;

    public ChatMessageAdapter(Context context, List<Messages> Messages) {
        mContext = context;
        messages = Messages;
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public int getItemViewType(int position) {
        Messages message = messages.get(position);

        if (message.to_me == VIEW_TYPE_MESSAGE_SENT) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else if (message.to_me == VIEW_TYPE_MESSAGE_RECEIVED) {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        } else {
            return message.to_me;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;

        if (i == VIEW_TYPE_MESSAGE_SENT) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.sended_messages, viewGroup, false);
            return new SentMessageHolder(v);
        } else if (i == VIEW_TYPE_MESSAGE_RECEIVED) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.received_messages, viewGroup, false);
            return new ReceivedMessageHolder(v);
        } else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.non_messages, viewGroup, false);
            return new ReceivedMessageHolder(v);
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder messageViewHolder, int i) {

        switch (messageViewHolder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) messageViewHolder).bind(messages.get(messageViewHolder.getAdapterPosition()));
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) messageViewHolder).bind(messages.get(messageViewHolder.getAdapterPosition()));
        }

    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(Messages message) {
            messageText.setText(message.message);
            timeText.setText(message.createdAt);
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(Messages message) {
            messageText.setText(message.message);
            timeText.setText(message.createdAt);
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
