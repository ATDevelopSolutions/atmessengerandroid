package com.at_develop.atmessenger.RecyclerViews.Adapters;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;

/**
 *
 * Created by jgcapistran on 26/10/17.
 *
 */

public class ChatMessageContract {

    static final String PATH_MESSAGE = "messages";
    static final String PATH_EVENT = "event";
    static final String PATH_CHECKIN_CHECKOUT = "checkin_checkout";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + ChatDataProvider.CONTENT_AUTHORITY);

    private interface MesssagesColumns{
        String ID = BaseColumns._ID;
        String ID_BD = "id";
        String MESSSAGE = "message";
        String FK_USUARIO_SENDER = "fk_usuario_sender";
        String STATUS_MSJ_SENDER = "status_msj_sender";
        String FK_USUARIO_RECEIVER = "fk_usuario_receiver";
        String STATUS_MSJ_RECEIVER = "status_msj_receiver";
        String STATUS_READ_MSJ = "status_read_msj";
        String MESSAGE_SEND = "message_send";
    }


    public static class Message extends Base implements MesssagesColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MESSAGE).build();
        static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + PATH_MESSAGE;
        static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + PATH_MESSAGE;
        public static final String DEFAULT_SORT_ORDER = User_Messages_Schema.User_Messages_Database.TABLE_NAME+"."+Message.MESSAGE_SEND + " ASC";

        static Uri buildItemUri(long itemId) {
            return ContentUris.withAppendedId(CONTENT_URI, itemId);
        }
    }

    static abstract class Base {
        static String getItemId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }
}
