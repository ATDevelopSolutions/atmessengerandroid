package com.at_develop.atmessenger;

/**
 * Created by: jgcapistran on 10/09/17.
 * Developed by: AT Develop
 */

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Friends_Schema;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.WebServices.BaseResponseWS;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends BaseVolleyActivity implements View.OnClickListener {
    Button Login_btn;
    private EditText email_ac, pass_ac;
    TextView Register_btn;
    RelativeLayout progress;
    CircularProgressView progressView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_fragment_login, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        Register_btn = (TextView) v.findViewById(R.id.register_btn);
        Login_btn = (Button) v.findViewById(R.id.login_btn);
        email_ac = (EditText) v.findViewById(R.id.login_user);
        pass_ac = (EditText) v.findViewById(R.id.login_pass);
        Register_btn.setOnClickListener(this);
        Login_btn.setOnClickListener(this);

        progress = (RelativeLayout) v.findViewById(R.id.progress_back);
        progress.setVisibility(View.INVISIBLE);
        return v;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:

                String disp_id = "";

                try{
                    disp_id = FirebaseInstanceId.getInstance().getToken();
                }catch (Exception e){
                    Log.wtf("Error", e.toString());
                }

                if ((!email_ac.getText().toString().equals("")) && (!pass_ac.getText().toString().equals(""))) {
                    progress.setVisibility(View.VISIBLE);
                    makeRequest(v, email_ac.getText().toString(), pass_ac.getText().toString(), disp_id, progress);
                } else if ((email_ac.getText().toString().equals("")) && (!pass_ac.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getApplicationContext(), "Error, ingresa tu usuario", Toast.LENGTH_LONG).show();
                } else if ((!email_ac.getText().toString().equals("")) && (pass_ac.getText().toString().equals(""))) {
                    Toast.makeText(getActivity().getApplicationContext(), "Error, ingresa tu contraseña", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Error, ingresa tu usuario y contraseña", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.register_btn:
                Intent register = new Intent(getActivity().getApplicationContext(), RegisterActivity.class);
                startActivity(register);
                getActivity().overridePendingTransition(R.anim.animation_enter, R.anim.animation_end_enter);
                finishActivity();
                break;
        }
    }

    private void finishActivity() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }


    private void makeRequest(final View v, final String reg_email, final String reg_password, final String disp_id, final RelativeLayout progress) {

        String url = HelperWS.BASE_URL + "login";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Handler handler = new Handler();

                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                try {
                    if (responseServer.statusCode == 2) {

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                progress.setVisibility(View.INVISIBLE);
                                Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                            }
                        }, 3000);

                    } else if (responseServer.statusCode == 0) {

                        final HashMap obj = responseServer.resultData;
                        HelperWS helper = new HelperWS();

                        final int idUser = helper.parseIntResponse(obj, "idUser");
                        final int status_user = helper.parseIntResponse(obj, "statusUser");
                        final int status = helper.parseIntResponse(obj, "status");
                        final int sex = helper.parseIntResponse(obj, "sexo");
                        final int pais = helper.parseIntResponse(obj, "pais");
                        final int friends = helper.parseIntResponse(obj, "friends");
                        final int messages = helper.parseIntResponse(obj, "messages");
                        final String phone = helper.parseStringResponse(obj, "phone");
                        final String firstName = helper.parseStringResponse(obj, "firstName");
                        final String lastName = helper.parseStringResponse(obj, "lastName");
                        final String username = helper.parseStringResponse(obj, "username");
                        final String email = helper.parseStringResponse(obj, "email");
                        final String ValidacionAcceso = helper.parseStringResponse(obj, "ValidacionAcceso");
                        final String estadoUser = helper.parseStringResponse(obj, "estadoUser");
                        final String img_profile = helper.parseStringResponse(obj, "profileImg");

                        for (int i = 0; i < friends; i++) {

                            int idFriend = 0, status_friend = 0, friendStatus = 0, friendsex = 0, paisFriend = 0;
                            String friendfirstName = "", friendlastName = "", friendusername = "", friendemail = "", friendestadoUser = "", friendimg_profile = "", friendPhone = "";

                            JsonObject friend_array = responseServer.friendsData.get(i).getAsJsonObject();
                            idFriend = helper.parseIntJsonResponse(friend_array, "idUser");
                            friendPhone = helper.parseStringJsonResponse(friend_array, "phone");
                            status_friend = helper.parseIntJsonResponse(friend_array, "statusUser");
                            friendStatus = helper.parseIntJsonResponse(friend_array, "status");
                            friendsex = helper.parseIntJsonResponse(friend_array, "sexo");
                            paisFriend = helper.parseIntJsonResponse(friend_array, "pais");
                            friendfirstName = helper.parseStringJsonResponse(friend_array, "firstName");
                            friendlastName = helper.parseStringJsonResponse(friend_array, "lastName");
                            friendusername = helper.parseStringJsonResponse(friend_array, "username");
                            friendemail = helper.parseStringJsonResponse(friend_array, "email");
                            friendestadoUser = helper.parseStringJsonResponse(friend_array, "estadoUser");
                            friendimg_profile = helper.parseStringJsonResponse(friend_array, "profileImg");

                            // Insersión de amigos a la base de datos
                            User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(getActivity());
                            SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                            db.execSQL("CREATE TABLE IF NOT EXISTS " + User_Friends_Schema.User_Friends_Database.TABLE_NAME + " ("
                                    + User_Friends_Schema.User_Friends_Database._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    + User_Friends_Schema.User_Friends_Database.ID + " INTEGER NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.FIRST_NAME + " TEXT NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.LAST_NAME + " TEXT NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.USER_NAME + " TEXT NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.EMAIL + " TEXT NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.PROFILE_IMG + " TEXT,"
                                    + User_Friends_Schema.User_Friends_Database.STATUS + " INTEGER,"
                                    + User_Friends_Schema.User_Friends_Database.STATUS_USER + " INTEGER NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.ESTADO_USER + " TEXT,"
                                    + User_Friends_Schema.User_Friends_Database.SEX + " INTEGER NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.PAIS + " INTEGER NOT NULL,"
                                    + User_Friends_Schema.User_Friends_Database.PHONE + " TEXT,"
                                    + "UNIQUE (" + User_Friends_Schema.User_Friends_Database.ID + "))");

                            ContentValues nuevoRegistro = new ContentValues();

                            // Creacion del registro mediante un objeto de ContentValues
                            nuevoRegistro.put("id", idFriend);
                            nuevoRegistro.put("first_name", friendfirstName);
                            nuevoRegistro.put("last_name", friendlastName);
                            nuevoRegistro.put("username", friendusername);
                            nuevoRegistro.put("email", friendemail);
                            nuevoRegistro.put("profile_img", friendimg_profile);
                            nuevoRegistro.put("status", friendStatus);
                            nuevoRegistro.put("estado_user", friendestadoUser);
                            nuevoRegistro.put("status_user", status_friend);
                            nuevoRegistro.put("sex", friendsex);
                            nuevoRegistro.put("pais", paisFriend);
                            nuevoRegistro.put("phone", friendPhone);


                            // Insert nuevo registro con informacion del usuario
                            db.insert("at_mess_friends", null, nuevoRegistro);
                            db.close();

                        }

                        for (int i = 0; i < messages; i++) {

                            int idMessage = 0, userSender = 0, statusSender = 0, userReceiver = 0, statusReceiver = 0, statusRead = 0;
                            String Message = "", messageSend = "";

                            JsonObject messages_array = responseServer.messagesData.get(i).getAsJsonObject();
                            idMessage = helper.parseIntJsonResponse(messages_array, "idUser");
                            Message = helper.parseStringJsonResponse(messages_array, "message");
                            userSender = helper.parseIntJsonResponse(messages_array, "userSender");
                            statusSender = helper.parseIntJsonResponse(messages_array, "statusSender");
                            userReceiver = helper.parseIntJsonResponse(messages_array, "userReceiver");
                            statusReceiver = helper.parseIntJsonResponse(messages_array, "statusReceiver");
                            statusRead = helper.parseIntJsonResponse(messages_array, "statusRead");
                            messageSend = helper.parseStringJsonResponse(messages_array, "messageSend");

                            // Insersión de amigos a la base de datos
                            User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(getActivity());
                            SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                            db.execSQL("CREATE TABLE IF NOT EXISTS " + User_Messages_Schema.User_Messages_Database.TABLE_NAME + " ("
                                    + User_Messages_Schema.User_Messages_Database._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    + User_Messages_Schema.User_Messages_Database.ID + " INTEGER NOT NULL,"
                                    + User_Messages_Schema.User_Messages_Database.MESSSAGE + " TEXT NOT NULL,"
                                    + User_Messages_Schema.User_Messages_Database.FK_USUARIO_SENDER + " INTEGER NOT NULL,"
                                    + User_Messages_Schema.User_Messages_Database.STATUS_MSJ_SENDER + " INTEGER NOT NULL,"
                                    + User_Messages_Schema.User_Messages_Database.FK_USUARIO_RECEIVER + " INTEGER NOT NULL,"
                                    + User_Messages_Schema.User_Messages_Database.STATUS_MSJ_RECEIVER + " INTEGER,"
                                    + User_Messages_Schema.User_Messages_Database.STATUS_READ_MSJ + " INTEGER,"
                                    + User_Messages_Schema.User_Messages_Database.MESSAGE_SEND + " TEXT NOT NULL)");

                            ContentValues nuevoRegistro = new ContentValues();

                            // Creacion del registro mediante un objeto de ContentValues
                            nuevoRegistro.put("id", idMessage);
                            nuevoRegistro.put("message", Message);
                            nuevoRegistro.put("fk_usuario_sender", userSender);
                            nuevoRegistro.put("status_msj_sender", statusSender);
                            nuevoRegistro.put("fk_usuario_receiver", userReceiver);
                            nuevoRegistro.put("status_msj_receiver", statusReceiver);
                            nuevoRegistro.put("status_read_msj", statusRead);
                            nuevoRegistro.put("message_send", messageSend);

                            // Insert nuevo registro con informacion del usuario
                            db.insert("at_mess_messages", null, nuevoRegistro);
                            db.close();

                        }

                        // Conexion con la base de datos
                        User_Information_Helper create_database_user_information =
                                new User_Information_Helper(getActivity());
                        SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                        ContentValues nuevoRegistro = new ContentValues();

                        // Creacion del registro mediante un objeto de ContentValues
                        nuevoRegistro.put("id", idUser);
                        nuevoRegistro.put("first_name", firstName);
                        nuevoRegistro.put("last_name", lastName);
                        nuevoRegistro.put("username", username);
                        nuevoRegistro.put("email", email);
                        nuevoRegistro.put("profile_img", img_profile);
                        nuevoRegistro.put("status", status);
                        nuevoRegistro.put("estado_user", estadoUser);
                        nuevoRegistro.put("status_user", status_user);
                        nuevoRegistro.put("sex", sex);
                        nuevoRegistro.put("pais", pais);
                        nuevoRegistro.put("phone", phone);
                        nuevoRegistro.put("token_user", ValidacionAcceso);

                        // Insert nuevo registro con informacion del usuario
                        db.insert("at_mess_users", null, nuevoRegistro);
                        db.close();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                progress.setVisibility(View.INVISIBLE);
                                Intent login = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                                login.putExtra("us_id", String.valueOf(idUser));
                                login.putExtra("us_status", String.valueOf(status));
                                login.putExtra("us_status_user", String.valueOf(status_user));
                                login.putExtra("us_estado_user", obj.get("estadoUser").toString());
                                login.putExtra("us_first_name", obj.get("firstName").toString());
                                login.putExtra("us_last_name", obj.get("lastName").toString());
                                login.putExtra("us_email", obj.get("email").toString());
                                login.putExtra("us_username", obj.get("username").toString());
                                login.putExtra("us_profile_img", obj.get("profileImg").toString());
                                login.putExtra("us_sex", String.valueOf(sex));
                                login.putExtra("us_pais", String.valueOf(pais));
                                login.putExtra("us_cellphone", String.valueOf(phone));
                                login.putExtra("us_token", obj.get("ValidacionAcceso").toString());

                                startActivity(login);
                                getActivity().overridePendingTransition(R.anim.animation_enter, R.anim.animation_end_enter);
                                finishActivity();
                            }
                        }, 3000);

                    }
                } catch (Exception e) {
                    //Log.wtf("text", e.toString());
                }

                onConnectionFinished();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // error
                onConnectionFailed(String.valueOf(error));
                Toast.makeText(getActivity().getApplicationContext(), "Error de conexión al servidor.", Toast.LENGTH_LONG).show();

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", reg_email);
                params.put("pass", reg_password);
                params.put("clave_dispositivo", disp_id);
                params.put("tipo_disp", "1");
                return params;
            }
        };
        addToQueue(postRequest);
    }
}
