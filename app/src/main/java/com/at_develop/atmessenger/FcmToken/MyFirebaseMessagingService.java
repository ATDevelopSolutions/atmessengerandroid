package com.at_develop.atmessenger.FcmToken;

/**
 * Created by jgcapistran on 07/10/17.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.at_develop.atmessenger.R;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatDataProvider;
import com.at_develop.atmessenger.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        //Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            final Map<String, String> data = remoteMessage.getData();

            if(Integer.valueOf(data.get("mTipo")) == 1){

                ContentValues nuevoRegistro = new ContentValues();

                // Creacion del registro mediante un objeto de ContentValues
                nuevoRegistro.put("id", data.get("fId"));
                nuevoRegistro.put("first_name", data.get("f_name"));
                nuevoRegistro.put("last_name", data.get("f_lname"));
                nuevoRegistro.put("username", data.get("f_username"));
                nuevoRegistro.put("email", data.get("f_email"));
                nuevoRegistro.put("profile_img", data.get("f_profile_img"));
                nuevoRegistro.put("status", 0);
                nuevoRegistro.put("status_user", data.get("f_status_user"));
                nuevoRegistro.put("estado_user", data.get("f_estado_user"));
                nuevoRegistro.put("sex", data.get("f_sexo"));
                nuevoRegistro.put("pais", data.get("f_pais"));
                nuevoRegistro.put("phone", data.get("f_phone"));

                getApplicationContext().getContentResolver().insert(ChatDataProvider.CONTENT_URI_C, nuevoRegistro);

            }else {
                Log.wtf("dD","Data si entra");
                String message = data.get("message");

                sendNotification(message);

                ContentValues nuevoRegistro = new ContentValues();

                // Creacion del registro mediante un objeto de ContentValues
                nuevoRegistro.put("id", data.get("mId"));
                nuevoRegistro.put("message", data.get("mData"));
                nuevoRegistro.put("fk_usuario_sender", data.get("sender"));
                nuevoRegistro.put("status_msj_sender", data.get("statusSender"));
                nuevoRegistro.put("fk_usuario_receiver", data.get("receiver"));
                nuevoRegistro.put("status_msj_receiver", data.get("statusReceiver"));
                nuevoRegistro.put("status_read_msj", data.get("statusRead"));
                nuevoRegistro.put("message_send", data.get("createdAt"));

                getApplicationContext().getContentResolver().insert(ChatDataProvider.CONTENT_URI, nuevoRegistro);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Bodyn: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
       Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                //.setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
