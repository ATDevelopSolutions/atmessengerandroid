package com.at_develop.atmessenger.FuctionsHelpers;

import android.util.Log;

import com.at_develop.atmessenger.WebServices.FriendsResponseWS;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by jgcapistran on 07/10/17.
 */

public class HelperWS {

    public static String BASE_URL = "http://35.185.205.174/ATDevelop/";

    public int parseIntResponse(HashMap array_map, String key_map){
        double value_object = Double.valueOf(array_map.get(key_map).toString());
        int value = (int) value_object;
        return value;

    }

    public double parseDoubleResponse(HashMap array_map, String key_map){

        double value = Double.valueOf(array_map.get(key_map).toString());
        return value;
    }

    public String parseStringResponse(HashMap array_map, String key_map){

        String value = array_map.get(key_map).toString();

        if(value == null || value == "null"){
            value = "";
        }

        return value;
    }

    public HashMap parseHashResponse(HashMap array_map, String key_map){

        HashMap value = (HashMap) array_map.get(key_map);

        return value;
    }

    public Array parseArrayResponse(HashMap array_map, String key_map){
        Array value = (Array) array_map.get(key_map);
        return value;
    }

    public Object parseObjectResponse(HashMap array_map, String key_map){
        Object value = (Object) array_map.get(key_map);
        return value;
    }

    public List<Object> parseListObjectResponse(HashMap array_map, String key_map){
        List<Object> value = (List<Object>) array_map.get(key_map);
        return value;
    }

    //Funciones para JsonArray Gson
    public int parseIntJsonResponse(JsonObject array_map, String key_map){
        int value = array_map.get(key_map).getAsInt();
        return value;
    }

    public String parseStringJsonResponse(JsonObject array_map, String key_map){
        String value = array_map.get(key_map).getAsString();
        return value;
    }

    public String parseHourTimestampWZone(String timestamp){
        try {
            int indexDot = timestamp.indexOf('.');
            if(indexDot >0){
                timestamp = timestamp.substring(0,indexDot);
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date = dateFormat.parse(timestamp);
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String formatted = dateFormat2.format(date);
            return formatted;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
