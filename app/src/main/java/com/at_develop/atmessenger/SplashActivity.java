package com.at_develop.atmessenger;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;

/**
 * Created by: jgcapistran on 10/09/17.
 * Developed by: AT Develop
 */

public class SplashActivity extends AppCompatActivity {

    private final int DURACION_SPLASH  = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        Window w = getWindow(); // in Activity's onCreate() for instance
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(SplashActivity.this);
                SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

                // Nos aseguramos de que existe al menos un registro
                if (result_query.moveToFirst()) {
                    // Obtencion de los datos de usuario desde la base de datos
                    int id_base = result_query.getInt(0);
                    int id = result_query.getInt(1);
                    String first_name = result_query.getString(2);
                    String last_name = result_query.getString(3);
                    String user_name = result_query.getString(4);
                    String email = result_query.getString(5);
                    String profile_img = result_query.getString(6);
                    int status = result_query.getInt(7);
                    int status_user = result_query.getInt(8);
                    String estado_user = result_query.getString(9);
                    int sex = result_query.getInt(10);
                    int pais = result_query.getInt(11);
                    int phone = result_query.getInt(12);
                    String token_user = result_query.getString(13);

                    db.close();
                    Intent Principal_main = new Intent();
                    Principal_main.setClass(SplashActivity.this, MainActivity.class);
                    Principal_main.putExtra("us_id", String.valueOf(id));
                    Principal_main.putExtra("us_status", String.valueOf(status));
                    Principal_main.putExtra("us_status_user", status_user);
                    Principal_main.putExtra("us_estado_user", estado_user);
                    Principal_main.putExtra("us_first_name", first_name);
                    Principal_main.putExtra("us_last_name", last_name);
                    Principal_main.putExtra("us_email", email);
                    Principal_main.putExtra("us_username", user_name);
                    Principal_main.putExtra("us_profile_img", profile_img);
                    Principal_main.putExtra("us_sex", String.valueOf(sex));
                    Principal_main.putExtra("us_pais", String.valueOf(pais));
                    Principal_main.putExtra("us_cellphone", String.valueOf(phone));
                    Principal_main.putExtra("us_token", token_user);


                    try {
                        finishAffinity();
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                        Principal_main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Principal_main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Principal_main.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        Principal_main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                    }
                    startActivity(Principal_main);
                }else{
                    Intent intent = new Intent();
                    intent.setClass(SplashActivity.this, LoginActivity.class);

                    try {
                        finishAffinity();
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                    }
                    startActivity(intent);
                }

            }
        },DURACION_SPLASH);
    }
}
