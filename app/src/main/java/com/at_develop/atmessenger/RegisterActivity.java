package com.at_develop.atmessenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by: jgcapistran on 10/09/17.
 * Developed by: AT Develop
 */

public class RegisterActivity extends AppCompatActivity{
    Button return_login, register_next;
    EditText register_username, register_email, register_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        register_username = (EditText) findViewById(R.id.register_username);
        register_email = (EditText) findViewById(R.id.register_email);
        register_password = (EditText) findViewById(R.id.register_pass);

        try{
            String reg_username = getIntent().getExtras().getString("reg_username");
            String reg_email = getIntent().getExtras().getString("reg_email");
            String reg_password = getIntent().getExtras().getString("reg_password");

            if(!reg_username.isEmpty() && !reg_email.isEmpty() && !reg_password.isEmpty()) {
                register_username.setText(reg_username);
                register_email.setText(reg_email);
                register_password.setText(reg_password);
            }

        }catch (Exception e){
            Log.d("Datos",e.toString());
        }

        return_login = (Button) findViewById(R.id.register_return_login_btn);
        return_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(login);
                overridePendingTransition(R.anim.animation_leave, R.anim.animation_end_leave);
                finish();
            }
        });

        register_next = (Button) findViewById(R.id.register_next_btn);
        register_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(getApplicationContext(), Register_2_Activity.class);
                register.putExtra("reg_username", register_username.getText().toString());
                register.putExtra("reg_email", register_email.getText().toString());
                register.putExtra("reg_password", register_password.getText().toString());
                startActivity(register);
                overridePendingTransition(R.anim.animation_enter, R.anim.animation_end_enter);
                finish();
            }
        });

    }
}
