package com.at_develop.atmessenger;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Friends_Schema;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ContactsContract;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ContactsDataAdapter;
import com.at_develop.atmessenger.RecyclerViews.Adapters.FriendAdapter;
import com.at_develop.atmessenger.RecyclerViews.Adapters.Friends;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by: jgcapistran on 14/09/17.
 * Developed by: AT Develop
 */

public class ChatsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    int us_id, status, status_user, sex, pais, phone;
    String first_name, last_name, user_name, email, estado_user, token_user, profile_img;
    TextView profile_username, profile_estado_user;
    ImageView profile_user_img;
    LinearLayout content_user_image;
    private ContactsDataAdapter ContactsAdapter;
    RecyclerView mContactsRecycler;

    public static ChatsFragment newInstance() {
        ChatsFragment fragment = new ChatsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_fragment_chats, container, false);


        us_id = getActivity().getIntent().getExtras().getInt("us_id");
        status = getActivity().getIntent().getExtras().getInt("us_status");
        status_user = getActivity().getIntent().getExtras().getInt("us_status_user");
        estado_user = getActivity().getIntent().getExtras().getString("us_estado_user");
        first_name = getActivity().getIntent().getExtras().getString("us_first_name");
        last_name = getActivity().getIntent().getExtras().getString("us_last_name");
        email = getActivity().getIntent().getExtras().getString("us_email");
        user_name = getActivity().getIntent().getExtras().getString("us_username");
        profile_img = getActivity().getIntent().getExtras().getString("us_profile_img");
        sex = getActivity().getIntent().getExtras().getInt("us_sex");
        pais = getActivity().getIntent().getExtras().getInt("us_pais");
        phone = getActivity().getIntent().getExtras().getInt("us_cellphone");
        token_user = getActivity().getIntent().getExtras().getString("us_token");

        profile_user_img = (ImageView) v.findViewById(R.id.profile_user_img);

        // Asignación de Información del usuario logeado
        profile_username = (TextView) v.findViewById(R.id.profile_user_name);
        profile_username.setText(user_name);

        profile_estado_user = (TextView) v.findViewById(R.id.profile_estado_user);
        if (estado_user.equals("") || estado_user.equals("null")) {
            estado_user = getResources().getString(R.string.estado_default);
        }
        profile_estado_user.setText(estado_user);

        content_user_image = (LinearLayout) v.findViewById(R.id.profile_status_user);

        switch (status_user) {
            case 0:
                content_user_image.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.desconectado), getResources().getColor(R.color.desconectado)}
                ));
                content_user_image.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 1:
                content_user_image.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.disponible), getResources().getColor(R.color.disponible)}
                ));
                content_user_image.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 2:
                content_user_image.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.nodisponible), getResources().getColor(R.color.nodisponible)}
                ));
                content_user_image.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 3:
                content_user_image.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.ausente), getResources().getColor(R.color.ausente)}
                ));
                content_user_image.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
        }

        Glide.with(v).load(profile_img)
                .apply(bitmapTransform(new CropCircleTransformation()))
                .into(profile_user_img);

        mContactsRecycler = v.findViewById(R.id.my_recycler_view);
        mContactsRecycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        mContactsRecycler.setLayoutManager(llm);
        ContactsAdapter = new ContactsDataAdapter(getActivity().getApplicationContext(), null, ContactsAdapter.FLAG_AUTO_REQUERY);
        ContactsAdapter.notifyDataSetChanged();
        mContactsRecycler.setAdapter(ContactsAdapter);

        getLoaderManager().initLoader(0, null, this).forceLoad();

        return v;

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity().getApplicationContext(), ContactsContract.Contacts.CONTENT_URI, User_Friends_Schema.User_Friends_Database.ALL_FIELDS, null, null, ContactsContract.Contacts.DEFAULT_SORT_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if (data != null && data.moveToFirst()) {
            ContactsAdapter.swapCursor(data);
            ContactsAdapter.notifyDataSetChanged();
            mContactsRecycler.smoothScrollToPosition(data.getCount());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        ContactsAdapter.swapCursor(null);
    }


}