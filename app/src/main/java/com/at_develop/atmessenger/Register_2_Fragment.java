package com.at_develop.atmessenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by: jgcapistran on 19/09/17.
 * Developed by: AT Develop
 */

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.WebServices.BaseResponseWS;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class Register_2_Fragment extends BaseVolleyActivity {
    Button return_register, register_finish;
    String reg_username, reg_email, reg_password;
    EditText reg_name, reg_lname, reg_phone;
    Spinner reg_pais, reg_sex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_fragment_register_2, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        reg_username = getActivity().getIntent().getExtras().getString("reg_username");
        reg_email = getActivity().getIntent().getExtras().getString("reg_email");
        reg_password = getActivity().getIntent().getExtras().getString("reg_password");

        reg_name = (EditText) v.findViewById(R.id.register_name);
        reg_lname = (EditText) v.findViewById(R.id.register_lname);
        reg_phone = (EditText) v.findViewById(R.id.register_phone);
        reg_pais = (Spinner) v.findViewById(R.id.reg_pais_user);
        reg_sex = (Spinner) v.findViewById(R.id.reg_sex_user);

        return_register = (Button) v.findViewById(R.id.return_register_btn);
        return_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register1 = new Intent(getActivity().getApplicationContext(), RegisterActivity.class);
                register1.putExtra("reg_username", reg_username);
                register1.putExtra("reg_email", reg_email);
                register1.putExtra("reg_password", reg_password);
                startActivity(register1);
                getActivity().overridePendingTransition(R.anim.animation_leave, R.anim.animation_end_leave);
                finishActivity();
            }
        });

        register_finish = (Button) v.findViewById(R.id.register_finish_btn);
        register_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((!reg_email.equals("")) && (!reg_password.equals("")) && (!reg_username.equals("")) && (!reg_name.getText().toString().equals("")) && (!reg_lname.getText().toString().equals("")) && (!reg_phone.getText().toString().equals("")) && (!String.valueOf(reg_pais.getSelectedItemId()).equals("")) && (!String.valueOf(reg_sex.getSelectedItemId()).equals(""))) {
                    makeRequest(v, reg_email, reg_password, reg_username, reg_name.getText().toString(), reg_lname.getText().toString(), reg_phone.getText().toString(), String.valueOf(reg_pais.getSelectedItemId()), String.valueOf(reg_sex.getSelectedItemId()));
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Error, ingrese todos los campos.", Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;
    }

    private void finishActivity() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    private void makeRequest(final View v, final String reg_email, final String reg_password, final String reg_username, final String reg_name, final String reg_lname, final String reg_phone, final String reg_pais, final String reg_sex) {

        String url = HelperWS.BASE_URL+"register";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                String message = null;
                int statusCode = 0;

                BaseResponseWS responseServer = gson.fromJson(response, BaseResponseWS.class);
                statusCode = responseServer.statusCode;
                message = responseServer.statusMessage;


                if (statusCode == 9 || statusCode == 8) {

                    Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                } else if (statusCode == 2) {

                    Toast.makeText(getActivity().getApplicationContext(), "Cuenta registrada correctamente.", Toast.LENGTH_LONG).show();

                    Intent login = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                    startActivity(login);
                    getActivity().overridePendingTransition(R.anim.animation_enter, R.anim.animation_end_enter);
                    finishActivity();

                }

                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // error
                //onConnectionFailed(String.valueOf(error));
                Toast.makeText(getActivity().getApplicationContext(), "Error de conexión al servidor.", Toast.LENGTH_LONG).show();

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", reg_email);
                params.put("pass", reg_password);
                params.put("username", reg_username);
                params.put("fname", reg_name);
                params.put("lname", reg_lname);
                params.put("phone", reg_phone);
                params.put("pais", reg_pais);
                params.put("sex", reg_sex);
                return params;
            }
        };
        addToQueue(postRequest);
    }
}