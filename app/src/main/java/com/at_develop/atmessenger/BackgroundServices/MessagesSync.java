package com.at_develop.atmessenger.BackgroundServices;

import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.accounts.Account;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.LoginActivity;
import com.at_develop.atmessenger.ParseableData.MessageItem;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatDataProvider;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatMessageContract;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatMessageProvider;
import com.at_develop.atmessenger.WebServices.BaseResponseWS;
import com.cloudsourceit.cloudtools.network.JSONArray;
import com.cloudsourceit.cloudtools.network.JSONObject;
import com.cloudsourceit.cloudtools.network.NetworkTools;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.toolbox.Volley.newRequestQueue;

/**
 * Created by cloudsourceit on 26/10/17.
 */

public class MessagesSync extends AbstractThreadedSyncAdapter {

    private RequestQueue reQueue;

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public MessagesSync(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public MessagesSync(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     * .
     * <p>
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     * <p>
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */

    @SuppressWarnings("unchecked")
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        try {
            ArrayList<ContentProviderOperation> batchAdd = new ArrayList<>();
            ArrayList<ContentProviderOperation> batchDelete = new ArrayList<>();

            User_Information_Helper create_database_user_information =
                    new User_Information_Helper(getContext());
            final SQLiteDatabase db = create_database_user_information.getWritableDatabase();

            Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

            String token_user = "";
            // Nos aseguramos de que existe al menos un registro
            if (result_query.moveToFirst()) {
                token_user = result_query.getString(13);
            }

            db.close();
            final Bundle data = getDataEvent(batchAdd, batchDelete, getContext(), token_user);

            int operationResult = data.getInt(NetworkTools.OperationStatus.KEY_STATUS);

            final ContentResolver mContentResolver = getContext().getContentResolver();
            mContentResolver.applyBatch(ChatDataProvider.CONTENT_AUTHORITY, batchDelete);
            mContentResolver.applyBatch(ChatDataProvider.CONTENT_AUTHORITY, batchAdd);

        } catch (NullPointerException | RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }
    }


    private Bundle getDataEvent(final ArrayList<ContentProviderOperation> batchAdd, final ArrayList<ContentProviderOperation> batchDelete, final Context context, final String token_user) {

        final Bundle bundle = new Bundle();

        String url = HelperWS.BASE_URL + "newmessages";

        reQueue = newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 11) {

                    //Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                } else if (responseServer.statusCode == 0) {

                    HashMap obj = responseServer.resultData;
                    HelperWS helper = new HelperWS();

                    int newMessages = helper.parseIntResponse(obj, "new_msj");

                    if (newMessages == 1) {
                        getNewMessageRequest(bundle, batchAdd, batchDelete, context, token_user, obj);
                    }

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(context);
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.close();

                    Toast.makeText(context.getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(context.getApplicationContext(), LoginActivity.class);
                    context.startActivity(intent);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_receiver", String.valueOf(me_id));
                params.put("id_sender", String.valueOf(friend_id));
                return params;
            }*/

        };
        reQueue.add(request);
        return null;
    }

    private void getNewMessageRequest(final Bundle bundle, final ArrayList<ContentProviderOperation> batchAdd, final ArrayList<ContentProviderOperation> batchDelete,final Context context, final String token_user, final HashMap obj) {

        String url = HelperWS.BASE_URL + "getmessages";

        reQueue = newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 9) {
                    Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();
                } else if (responseServer.statusCode == 0) {

                    try {

                        HelperWS helper = new HelperWS();
                        final ArrayList<MessageItem> messageItems = new ArrayList<>();


                        for (int i = 0; i < Integer.valueOf(responseServer.statusMessage); i++) {

                            int idMessage = 0, userSender = 0, statusSender = 0, userReceiver = 0, statusReceiver = 0, statusRead = 0;
                            String Message = "", messageSend = "";

                            JsonObject messages_array = responseServer.messagesData.get(i).getAsJsonObject();

                            idMessage = helper.parseIntJsonResponse(messages_array, "idUser");
                            Message = helper.parseStringJsonResponse(messages_array, "message");
                            userSender = helper.parseIntJsonResponse(messages_array, "userSender");
                            statusSender = helper.parseIntJsonResponse(messages_array, "statusSender");
                            userReceiver = helper.parseIntJsonResponse(messages_array, "userReceiver");
                            statusReceiver = helper.parseIntJsonResponse(messages_array, "statusReceiver");
                            statusRead = helper.parseIntJsonResponse(messages_array, "statusRead");
                            messageSend = helper.parseStringJsonResponse(messages_array, "messageSend");

                            MessageItem messageItem = new MessageItem(idMessage, Message, userSender, statusSender, userReceiver, statusReceiver, statusRead, messageSend);
                            messageItems.add(messageItem);

                            ContentProviderOperation.Builder operationBuilder;
                            operationBuilder = ContentProviderOperation.newInsert(ChatMessageContract.Message.CONTENT_URI);
                            operationBuilder
                                    .withValue(ChatMessageContract.Message.ID_BD, idMessage)
                                    .withValue(ChatMessageContract.Message.MESSSAGE, Message.equals("null") ? "" : Message)
                                    .withValue(ChatMessageContract.Message.FK_USUARIO_SENDER, userSender)
                                    .withValue(ChatMessageContract.Message.STATUS_MSJ_SENDER, statusSender)
                                    .withValue(ChatMessageContract.Message.FK_USUARIO_RECEIVER, userReceiver)
                                    .withValue(ChatMessageContract.Message.STATUS_MSJ_RECEIVER, statusReceiver)
                                    .withValue(ChatMessageContract.Message.STATUS_READ_MSJ, statusRead)
                                    .withValue(ChatMessageContract.Message.MESSAGE_SEND, messageSend.equals("null") ? "" : messageSend);
                            batchAdd.add(operationBuilder.build());
                        }

                        //delete all messages
                        batchDelete.add(ContentProviderOperation.newDelete(ChatMessageContract.Message.CONTENT_URI).withSelection("", null).build());
                        bundle.putParcelableArrayList(User_Messages_Schema.User_Messages_Database.TABLE_NAME, messageItems);

                    } catch (Exception e) {
                        Log.wtf("error de cursor", e.toString());
                    }

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(context);
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.close();

                    Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(context, LoginActivity.class);
                    context.startActivity(intent);

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_receiver", String.valueOf(me_id));
                params.put("id_sender", String.valueOf(friend_id));
                return params;
            }*/

        };
        reQueue.add(request);
    }

}