package com.at_develop.atmessenger.BackgroundServices;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.LoginActivity;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatDataProvider;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatMessageAdapter;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatMessageProvider;
import com.at_develop.atmessenger.RecyclerViews.Adapters.Messages;
import com.at_develop.atmessenger.WebServices.BaseResponseWS;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.android.volley.toolbox.Volley.newRequestQueue;


public class CheckNewMessages extends Service {

    private RequestQueue reQueue;
    private Timer timer = new Timer();
    //List<Messages> messageList;

    @Override
    public void onCreate() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                /*Toast.makeText(getApplicationContext(), "Servicio creado",
                        Toast.LENGTH_SHORT).show();*/
                User_Information_Helper create_database_user_information =
                        new User_Information_Helper(getApplicationContext());
                final SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

                String token_user = "";
                // Nos aseguramos de que existe al menos un registro
                if (result_query.moveToFirst()) {
                    token_user = result_query.getString(13);
                }

                db.close();

                checkNewMessageRequest(getApplicationContext(), token_user);

            }
        }, 0, 5 * 1000);//5 Minutes
    }

    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {
        /*Toast.makeText(this, "Servicio arrancado " + idArranque,
                Toast.LENGTH_SHORT).show();
                */
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                /*Toast.makeText(getApplicationContext(), "Servicio creado",
                        Toast.LENGTH_SHORT).show();*/
                User_Information_Helper create_database_user_information =
                        new User_Information_Helper(getApplicationContext());
                final SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

                String token_user = "";
                // Nos aseguramos de que existe al menos un registro
                if (result_query.moveToFirst()) {
                    token_user = result_query.getString(13);
                }

                db.close();

                checkNewMessageRequest(getApplicationContext(), token_user);

            }
        }, 0, 5 * 1000);//5 Minutes
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Servicio detenido",
                Toast.LENGTH_SHORT).show();
        timer.cancel();
    }

    @Override
    public IBinder onBind(Intent intencion) {
        return null;
    }

    private void checkNewMessageRequest(final Context context, final String token_user) {

        String url = HelperWS.BASE_URL + "newmessages";

        reQueue = newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 11) {

                    Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                } else if (responseServer.statusCode == 0) {

                    HashMap obj = responseServer.resultData;
                    HelperWS helper = new HelperWS();

                    int newMessages = helper.parseIntResponse(obj, "new_msj");

                    if (newMessages == 1) {
                        Toast.makeText(getApplication().getApplicationContext(), "nuevo mensaje", Toast.LENGTH_LONG).show();
                        getNewMessageRequest(context, token_user);
                    }

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(context);
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.close();

                    Toast.makeText(getApplication().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(getApplication().getApplicationContext(), LoginActivity.class);
                    startActivity(intent);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_receiver", String.valueOf(me_id));
                params.put("id_sender", String.valueOf(friend_id));
                return params;
            }*/

        };
        reQueue.add(request);
    }

    private void getNewMessageRequest(final Context context, final String token_user) {

        String url = HelperWS.BASE_URL + "getmessages";

        reQueue = newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 9) {
                    Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();
                } else if (responseServer.statusCode == 0) {
                    //messageList = new ArrayList<>();

                    //Cursor nameListCursor = null;
                    try {
                        HelperWS helper = new HelperWS();

                        for (int i = 0; i < Integer.valueOf(responseServer.statusMessage); i++) {

                            int idMessage = 0, userSender = 0, statusSender = 0, userReceiver = 0, statusReceiver = 0, statusRead = 0;
                            String Message = "", messageSend = "";

                            Log.wtf("cursor", responseServer.messagesData.toString());

                            JsonObject messages_array = responseServer.messagesData.get(i).getAsJsonObject();
                            idMessage = helper.parseIntJsonResponse(messages_array, "idUser");
                            Message = helper.parseStringJsonResponse(messages_array, "message");
                            userSender = helper.parseIntJsonResponse(messages_array, "userSender");
                            statusSender = helper.parseIntJsonResponse(messages_array, "statusSender");
                            userReceiver = helper.parseIntJsonResponse(messages_array, "userReceiver");
                            statusReceiver = helper.parseIntJsonResponse(messages_array, "statusReceiver");
                            statusRead = helper.parseIntJsonResponse(messages_array, "statusRead");
                            messageSend = helper.parseStringJsonResponse(messages_array, "messageSend");


                            ContentValues nuevoRegistro = new ContentValues();

                            // Creacion del registro mediante un objeto de ContentValues
                            nuevoRegistro.put("id", idMessage);
                            nuevoRegistro.put("message", Message);
                            nuevoRegistro.put("fk_usuario_sender", userSender);
                            nuevoRegistro.put("status_msj_sender", statusSender);
                            nuevoRegistro.put("fk_usuario_receiver", userReceiver);
                            nuevoRegistro.put("status_msj_receiver", statusReceiver);
                            nuevoRegistro.put("status_read_msj", statusRead);
                            nuevoRegistro.put("message_send", messageSend);

                            //nameListCursor = ;
                            Log.wtf("status", responseServer.statusMessage);
                            context.getContentResolver().insert(ChatDataProvider.CONTENT_URI, nuevoRegistro);
                        }
                    } catch (Exception e) {
                        Log.wtf("error de cursor", e.toString());
                    }

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(context);
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.close();

                    Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(context, LoginActivity.class);
                    startActivity(intent);

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_receiver", String.valueOf(me_id));
                params.put("id_sender", String.valueOf(friend_id));
                return params;
            }*/

        };
        reQueue.add(request);
    }

}