package com.at_develop.atmessenger.BackgroundServices;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by jgcapistran on 27/10/17.
 */

public class MessagesSyncServices extends Service {

    private static final String TAG = "SyncService";
    private static final Object sSyncAdapterLock = new Object();
    private static MessagesSync sSyncAdapter = null;

    /**
     * Thread-safe constructor, creates static {@link MessagesSync} instance.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service created");
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new MessagesSync(getApplicationContext(), true);
            }
        }
    }

    @Override
/**
 * Logging-only destructor.
 */
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service destroyed");
    }

    /**
     * Return Binder handle for IPC communication with {@link MessagesSync}.
     * <p>
     * <p>New sync requests will be sent directly to the SyncAdapter using this channel.
     *
     * @param intent Calling intent
     * @return Binder handle for {@link MessagesSync}
     */
    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}