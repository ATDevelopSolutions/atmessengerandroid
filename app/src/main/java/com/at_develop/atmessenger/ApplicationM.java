package com.at_develop.atmessenger;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;

/**
 * Created by cloudsourceit on 27/10/17.
 */

public class ApplicationM extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}