package com.at_develop.atmessenger.ParseableData;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jgcapistran on 31/10/17.
 */

public class FriendItem implements Parcelable {

    int id;
    String user_name;
    String profile_img;
    int status_user;
    String estado_user;

    public FriendItem(int id, String user_name, String profile_img, int status_user, String estado_user) {
        this.id = id;
        this.user_name = user_name;
        this.profile_img = profile_img;
        this.status_user = status_user;
        this.estado_user = estado_user;
    }

    public int getId() {
        return id;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public int getStatus_user() {
        return status_user;
    }

    public String getEstado_user() {
        return estado_user;
    }


    private FriendItem(Parcel in) {
        id = in.readInt();
        user_name = in.readString();
        profile_img = in.readString();
        status_user = in.readInt();
        estado_user = in.readString();
    }

    public static final Creator<FriendItem> CREATOR = new Creator<FriendItem>() {
        @Override
        public FriendItem createFromParcel(Parcel in) {
            return new FriendItem(in);
        }

        @Override
        public FriendItem[] newArray(int size) {
            return new FriendItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(user_name);
        dest.writeString(profile_img);
        dest.writeInt(status_user);
        dest.writeString(estado_user);
    }
}