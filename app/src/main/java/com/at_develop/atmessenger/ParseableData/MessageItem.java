package com.at_develop.atmessenger.ParseableData;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jgcapistran on 26/10/17.
 */

public class MessageItem implements Parcelable {

    int id;
    String message;
    int fk_usuario_sender;
    int status_msj_sender;
    int fk_usuario_receiver;
    int status_msj_receiver;
    int status_read_msj;
    String message_send;

    public MessageItem(int id, String message, int fk_usuario_sender, int status_msj_sender, int fk_usuario_receiver, int status_msj_receiver, int status_read_msj, String message_send) {
        this.id = id;
        this.message = message;
        this.fk_usuario_sender = fk_usuario_sender;
        this.status_msj_sender = status_msj_sender;
        this.fk_usuario_receiver = fk_usuario_receiver;
        this.status_msj_receiver = status_msj_receiver;
        this.status_read_msj = status_read_msj;
        this.message_send = message_send;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public int getFk_usuario_sender() {
        return fk_usuario_sender;
    }

    public int getStatus_msj_sender() {
        return status_msj_sender;
    }

    public int getFk_usuario_receiver() {
        return fk_usuario_receiver;
    }

    public int getStatus_msj_receiver() {
        return status_msj_receiver;
    }

    public int getStatus_read_msj() {
        return status_read_msj;
    }

    public String getMessage_send() {
        return message_send;
    }


    private MessageItem(Parcel in) {
        id = in.readInt();
        message = in.readString();
        fk_usuario_sender = in.readInt();
        status_msj_sender = in.readInt();
        fk_usuario_receiver = in.readInt();
        status_msj_receiver = in.readInt();
        status_read_msj = in.readInt();
        message_send = in.readString();
    }

    public static final Creator<MessageItem> CREATOR = new Creator<MessageItem>() {
        @Override
        public MessageItem createFromParcel(Parcel in) {
            return new MessageItem(in);
        }

        @Override
        public MessageItem[] newArray(int size) {
            return new MessageItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(message);
        dest.writeInt(fk_usuario_sender);
        dest.writeInt(status_msj_sender);
        dest.writeInt(fk_usuario_receiver);
        dest.writeInt(status_msj_receiver);
        dest.writeInt(status_read_msj);
        dest.writeString(message_send);
    }
}