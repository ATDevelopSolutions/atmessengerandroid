package com.at_develop.atmessenger.Database.Schema;

import android.provider.BaseColumns;

/**
 * Created by jgcapistran on 16/10/17.
 */

public class User_Messages_Schema {
    public static class User_Messages_Database implements BaseColumns {
        public static final String TABLE_NAME = "at_mess_messages";
        public static final String ID = "id";
        public static final String MESSSAGE = "message";
        public static final String FK_USUARIO_SENDER = "fk_usuario_sender";
        public static final String STATUS_MSJ_SENDER = "status_msj_sender";
        public static final String FK_USUARIO_RECEIVER = "fk_usuario_receiver";
        public static final String STATUS_MSJ_RECEIVER = "status_msj_receiver";
        public static final String STATUS_READ_MSJ = "status_read_msj";
        public static final String MESSAGE_SEND = "message_send";

        public static final String ALL_FIELDS[] = new String[]{_ID, ID, MESSSAGE, FK_USUARIO_SENDER, STATUS_MSJ_SENDER, FK_USUARIO_RECEIVER, STATUS_MSJ_RECEIVER, STATUS_READ_MSJ, MESSAGE_SEND};
    }
}