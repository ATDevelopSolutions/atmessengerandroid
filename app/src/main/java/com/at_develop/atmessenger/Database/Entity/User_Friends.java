package com.at_develop.atmessenger.Database.Entity;

/**
 * Created by jgcapistran on 25/09/17.
 */

public class User_Friends {
    private int id;
    private String first_name;
    private String last_name;
    private String email;
    private String username;
    private String phone;
    private String profile_img;
    private String estado_user;
    private int sex;
    private int pais;
    private int status_user;
    private int status;

    public User_Friends (int id, String first_name, String last_name, String email,
                             String profile_img, String estado_user, int status_user,
                             int status, int sex, int pais, String phone, String username) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.email = email;
        this.profile_img = profile_img;
        this.status = status;
        this.status_user = status_user;
        this.estado_user = estado_user;
        this.sex = sex;
        this.pais = pais;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public String getEstado_user() {
        return estado_user;
    }

    public int getStatus_user() {
        return status_user;
    }

    public int getStatus() {
        return status;
    }

    public int getSex() {
        return sex;
    }

    public int getPais() {
        return pais;
    }

    public String getPhone() {
        return phone;
    }
}
