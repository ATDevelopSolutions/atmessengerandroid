package com.at_develop.atmessenger.Database.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.at_develop.atmessenger.Database.Schema.User_Friends_Schema;
import com.at_develop.atmessenger.Database.Schema.User_Information_Schema.*;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;

/**
 * Created by jgcapistran on 25/09/17.
 */

public class User_Information_Helper extends SQLiteOpenHelper {

    private static User_Information_Helper mInstance = null;
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "ATDev_ATMessenger.db";

    public static User_Information_Helper getInstance(Context ctx) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new User_Information_Helper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public User_Information_Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + User_Information_Database.TABLE_NAME + " ("
                + User_Information_Database._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + User_Information_Database.ID + " INTEGER NOT NULL,"
                + User_Information_Database.FIRST_NAME + " TEXT NOT NULL,"
                + User_Information_Database.LAST_NAME + " TEXT NOT NULL,"
                + User_Information_Database.USER_NAME + " TEXT NOT NULL,"
                + User_Information_Database.EMAIL + " TEXT NOT NULL,"
                + User_Information_Database.PROFILE_IMG + " TEXT,"
                + User_Information_Database.STATUS + " INTEGER,"
                + User_Information_Database.STATUS_USER + " INTEGER NOT NULL,"
                + User_Information_Database.ESTADO_USER + " TEXT,"
                + User_Information_Database.SEX + " INTEGER NOT NULL,"
                + User_Information_Database.PAIS + " INTEGER NOT NULL,"
                + User_Information_Database.PHONE + " INTEGER,"
                + User_Information_Database.VALIDACIONACCESO + " TEXT,"
                + "UNIQUE (" + User_Information_Database.ID + "))");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + User_Messages_Schema.User_Messages_Database.TABLE_NAME + " ("
                + User_Messages_Schema.User_Messages_Database._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + User_Messages_Schema.User_Messages_Database.ID + " INTEGER NOT NULL,"
                + User_Messages_Schema.User_Messages_Database.MESSSAGE + " TEXT NOT NULL,"
                + User_Messages_Schema.User_Messages_Database.FK_USUARIO_SENDER + " INTEGER NOT NULL,"
                + User_Messages_Schema.User_Messages_Database.STATUS_MSJ_SENDER + " INTEGER NOT NULL,"
                + User_Messages_Schema.User_Messages_Database.FK_USUARIO_RECEIVER + " INTEGER NOT NULL,"
                + User_Messages_Schema.User_Messages_Database.STATUS_MSJ_RECEIVER + " INTEGER,"
                + User_Messages_Schema.User_Messages_Database.STATUS_READ_MSJ + " INTEGER,"
                + User_Messages_Schema.User_Messages_Database.MESSAGE_SEND + " TEXT NOT NULL)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + User_Friends_Schema.User_Friends_Database.TABLE_NAME + " ("
                + User_Friends_Schema.User_Friends_Database._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + User_Friends_Schema.User_Friends_Database.ID + " INTEGER NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.FIRST_NAME + " TEXT NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.LAST_NAME + " TEXT NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.USER_NAME + " TEXT NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.EMAIL + " TEXT NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.PROFILE_IMG + " TEXT,"
                + User_Friends_Schema.User_Friends_Database.STATUS + " INTEGER,"
                + User_Friends_Schema.User_Friends_Database.STATUS_USER + " INTEGER NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.ESTADO_USER + " TEXT,"
                + User_Friends_Schema.User_Friends_Database.SEX + " INTEGER NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.PAIS + " INTEGER NOT NULL,"
                + User_Friends_Schema.User_Friends_Database.PHONE + " TEXT,"
                + "UNIQUE (" + User_Friends_Schema.User_Friends_Database.ID + "))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
