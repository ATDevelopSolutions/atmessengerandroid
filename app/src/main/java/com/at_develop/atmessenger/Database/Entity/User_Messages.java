package com.at_develop.atmessenger.Database.Entity;

/**
 * Created by jgcapistran on 16/10/17.
 */

public class User_Messages {
    private int id;
    private String message;
    private int fk_usuario_sender;
    private int status_msj_sender;
    private int fk_usuario_receiver;
    private int status_msj_receiver;
    private int status_read_msj;
    private String message_send;

    public User_Messages (int id, String message, int fk_usuario_sender, int status_msj_sender,
                         int fk_usuario_receiver, int status_msj_receiver, int status_read_msj,
                         String message_send) {
        this.id = id;
        this.message = message;
        this.fk_usuario_sender = fk_usuario_sender;
        this.status_msj_sender = status_msj_sender;
        this.fk_usuario_receiver = fk_usuario_receiver;
        this.status_msj_receiver = status_msj_receiver;
        this.status_read_msj = status_read_msj;
        this.message_send = message_send;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public int getFk_usuario_sender() {
        return fk_usuario_sender;
    }

    public int getStatus_msj_sender() {
        return status_msj_sender;
    }

    public int getFk_usuario_receiver() {
        return fk_usuario_receiver;
    }

    public int getStatus_msj_receiver() {
        return status_msj_receiver;
    }

    public int getStatus_read_msj() {
        return status_read_msj;
    }

    public String getMessage_send() {
        return message_send;
    }

}
