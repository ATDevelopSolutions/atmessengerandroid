package com.at_develop.atmessenger.Database.Schema;

import android.provider.BaseColumns;

/**
 * Created by jgcapistran on 07/10/17.
 */

public class User_Friends_Schema {
    public static abstract class User_Friends_Database implements BaseColumns {
        public static final String TABLE_NAME = "at_mess_friends";
        public static final String ID = "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String USER_NAME = "username";
        public static final String EMAIL = "email";
        public static final String PROFILE_IMG = "profile_img";
        public static final String STATUS = "status";
        public static final String STATUS_USER = "status_user";
        public static final String ESTADO_USER = "estado_user";
        public static final String SEX = "sex";
        public static final String PAIS = "pais";
        public static final String PHONE = "phone";

        public static final String ALL_FIELDS[] = new String[]{_ID, ID, FIRST_NAME, LAST_NAME, USER_NAME, EMAIL, PROFILE_IMG, STATUS, STATUS_USER, ESTADO_USER, SEX, PAIS, PHONE};

    }
}