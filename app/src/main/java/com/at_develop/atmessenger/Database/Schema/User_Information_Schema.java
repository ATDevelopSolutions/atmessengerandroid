package com.at_develop.atmessenger.Database.Schema;

import android.provider.BaseColumns;

/**
 * Created by jgcapistran on 25/09/17.
 */


public class User_Information_Schema {
    public static abstract class User_Information_Database implements BaseColumns {
        public static final String TABLE_NAME = "at_mess_users";
        public static final String ID = "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String USER_NAME = "username";
        public static final String EMAIL = "email";
        public static final String PROFILE_IMG = "profile_img";
        public static final String STATUS = "status";
        public static final String STATUS_USER = "status_user";
        public static final String ESTADO_USER = "estado_user";
        public static final String SEX = "sex";
        public static final String PAIS = "pais";
        public static final String PHONE = "phone";
        public static final String VALIDACIONACCESO = "token_user";
    }
}