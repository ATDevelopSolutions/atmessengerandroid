package com.at_develop.atmessenger;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Messages_Schema;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatDataAdapter;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatDataProvider;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatMessageContract.Message;
import com.at_develop.atmessenger.WebServices.BaseResponseWS;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by: jgcapistran on 16/09/17.
 * Developed by: AT Develop
 */

public class ChatMessageActivity extends BaseVolleyActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    int friend_id, friendStatusUser;
    String friendEstadoUser, friendUserName, friendProfileUrl, token_user;
    TextView FriendEstado, FriendUsername;
    ImageView FriendProfile;
    LinearLayout FriendContentImg, FriendContentProfile;
    EditText messageToSend;
    ImageButton sendMessage;
    private ChatDataAdapter chatMessagesAdapter;
    RecyclerView mMessageRecycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.activity_fragment_chat_message, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


        //Carga de información de la vista principal
        friendUserName = getActivity().getIntent().getExtras().getString("friend_user_name");
        friend_id = getActivity().getIntent().getExtras().getInt("friend_id");
        friendEstadoUser = getActivity().getIntent().getExtras().getString("friend_estado_user");
        friendStatusUser = getActivity().getIntent().getExtras().getInt("friend_status_user");
        friendProfileUrl = getActivity().getIntent().getExtras().getString("friend_img_profile");
        token_user = getActivity().getIntent().getExtras().getString("token_user");

        User_Information_Helper create_database_user_information =
                new User_Information_Helper(getActivity());
        final SQLiteDatabase db = create_database_user_information.getWritableDatabase();

        Cursor result_query = db.rawQuery("SELECT * FROM at_mess_users", null);

        String token_user = "";
        int id = 0;
        // Nos aseguramos de que existe al menos un registro
        if (result_query.moveToFirst()) {
            id = result_query.getInt(1);
            token_user = result_query.getString(13);
        }

        updateAllMessageRequest(v, id, token_user, friend_id);

        FriendUsername = v.findViewById(R.id.profile_user_name);
        FriendEstado = v.findViewById(R.id.profile_estado_user);
        FriendContentImg = v.findViewById(R.id.profile_status_user);
        FriendProfile = v.findViewById(R.id.profile_user_img);
        FriendContentProfile = v.findViewById(R.id.content_profile_img);
        sendMessage = v.findViewById(R.id.button_send_message);
        messageToSend = v.findViewById(R.id.message_to_send);

        //Uso de los elementos base del contacto
        FriendUsername.setText(friendUserName);
        FriendEstado.setText(friendEstadoUser);

        FriendContentProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        final String finalToken_user = token_user;

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRequest(v, messageToSend.getText().toString(), finalToken_user, friend_id);
            }
        });

        Glide.with(getActivity()).load(friendProfileUrl)
                .apply(bitmapTransform(new CropCircleTransformation()))
                .into(FriendProfile);

        switch (friendStatusUser) {
            case 0:
                FriendContentImg.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.desconectado), getResources().getColor(R.color.desconectado)}
                ));
                FriendContentImg.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 1:
                FriendContentImg.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.disponible), getResources().getColor(R.color.disponible)}
                ));
                FriendContentImg.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 2:
                FriendContentImg.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.nodisponible), getResources().getColor(R.color.nodisponible)}
                ));
                FriendContentImg.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
            case 3:
                FriendContentImg.setBackgroundTintList(new ColorStateList(
                        new int[][]{new int[]{android.R.attr.state_pressed},
                                new int[]{}
                        },
                        new int[]{getResources().getColor(R.color.ausente), getResources().getColor(R.color.ausente)}
                ));
                FriendContentImg.setBackgroundTintMode(PorterDuff.Mode.SRC_OVER);
                break;
        }

        v.findViewById(R.id.zumb_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.findViewById(R.id.zumb_btn).setBackgroundColor(getResources().getColor(R.color.desconectado));
                Vibrator vib = (Vibrator) getActivity().getSystemService(getActivity().getApplicationContext().VIBRATOR_SERVICE);
                vib.vibrate(2000);
                v.findViewById(R.id.zumb_btn).setBackgroundColor(getResources().getColor(R.color.backWhite));
            }
        });

        mMessageRecycler = v.findViewById(R.id.reyclerview_message_list);
        mMessageRecycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        mMessageRecycler.setLayoutManager(llm);
        chatMessagesAdapter = new ChatDataAdapter(getActivity().getApplicationContext(), null, ChatDataAdapter.FLAG_AUTO_REQUERY);
        chatMessagesAdapter.notifyDataSetChanged();
        mMessageRecycler.setAdapter(chatMessagesAdapter);

        Bundle data_query = new Bundle();
        data_query.putInt("friend_id", friend_id);
        data_query.putInt("me_id", id);
        getLoaderManager().initLoader(0, data_query, this).forceLoad();

        return v;

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity().getApplicationContext(), Message.CONTENT_URI, User_Messages_Schema.User_Messages_Database.ALL_FIELDS, "fk_usuario_receiver = ? OR fk_usuario_sender = ?", new String[]{ String.valueOf(args.getInt("friend_id")), String.valueOf(args.getInt("friend_id"))}, Message.DEFAULT_SORT_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null && data.moveToFirst()) {
            chatMessagesAdapter.swapCursor(data);
            chatMessagesAdapter.notifyDataSetChanged();
            mMessageRecycler.smoothScrollToPosition(data.getCount());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        chatMessagesAdapter.swapCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    private void makeRequest(final View v, final String message, final String token_user,
                             final int friend_id) {

        String url = HelperWS.BASE_URL + "sendmessage";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 2 || responseServer.statusCode == 8) {

                    Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                } else if (responseServer.statusCode == 0) {

                    messageToSend.setText("");

                    final HashMap obj = responseServer.resultData;
                    HelperWS helper = new HelperWS();

                    final int idMessage = helper.parseIntResponse(obj, "id");
                    final String Message = helper.parseStringResponse(obj, "message");
                    final int fk_usuario_sender = helper.parseIntResponse(obj, "fk_usuario_sender");
                    final int status_msj_sender = helper.parseIntResponse(obj, "status_msj_sender");
                    final int fk_usuario_receiver = helper.parseIntResponse(obj, "fk_usuario_receiver");
                    final int status_msj_receiver = helper.parseIntResponse(obj, "status_msj_receiver");
                    final int status_read_msj = helper.parseIntResponse(obj, "status_read_msj");
                    final String message_send = helper.parseStringResponse(obj, "message_send");
                    final int idMe = helper.parseIntResponse(obj, "id_user");

                    User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(getActivity());
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                    ContentValues nuevoRegistro = new ContentValues();

                    // Creacion del registro mediante un objeto de ContentValues
                    nuevoRegistro.put("id", idMessage);
                    nuevoRegistro.put("message", Message);
                    nuevoRegistro.put("fk_usuario_sender", fk_usuario_sender);
                    nuevoRegistro.put("status_msj_sender", status_msj_sender);
                    nuevoRegistro.put("fk_usuario_receiver", fk_usuario_receiver);
                    nuevoRegistro.put("status_msj_receiver", status_msj_receiver);
                    nuevoRegistro.put("status_read_msj", status_read_msj);
                    nuevoRegistro.put("message_send", message_send);

                    // Insert nuevo registro con informacion del usuario
                    getActivity().getContentResolver().insert(ChatDataProvider.CONTENT_URI, nuevoRegistro);

                    updateMessageRequest(v, idMe, token_user, friend_id);

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(getActivity().getApplicationContext());
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.delete("at_mess_messages", null, null);
                    db.delete("at_mess_friends", null, null);
                    db.close();

                    Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(getActivity().getApplicationContext(), LoginActivity.class);
                    getActivity().finish();
                    startActivity(intent);

                }

                onConnectionFinished();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity().getApplicationContext(), "Error de conexión al servidor.", Toast.LENGTH_LONG).show();
                onConnectionFinished();

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("message", message);
                params.put("friend_id", String.valueOf(friend_id));
                return params;
            }
        };
        addToQueue(postRequest);
    }

    private void updateMessageRequest(final View v, final int me_id, final String token_user,
                                      final int friend_id) {

        String url = HelperWS.BASE_URL + "updatemessage";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 10 || responseServer.statusCode == 8) {
                    Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();
                } else if (responseServer.statusCode == 0) {

                    ContentValues nuevoRegistro = new ContentValues();
                    nuevoRegistro.put("status_read_msj", 1);

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(getActivity());
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                    db.update("at_mess_messages", nuevoRegistro, "fk_usuario_receiver = ? AND fk_usuario_sender = ?", new String[]{String.valueOf(me_id), String.valueOf(friend_id)});

                    db.close();

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(getActivity().getApplicationContext());
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.delete("at_mess_messages", null, null);
                    db.delete("at_mess_friends", null, null);
                    db.close();

                    Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(getActivity().getApplicationContext(), LoginActivity.class);
                    getActivity().finish();
                    startActivity(intent);

                }

                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onConnectionFinished();

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_receiver", String.valueOf(me_id));
                params.put("id_sender", String.valueOf(friend_id));
                return params;
            }
        };
        addToQueue(postRequest);
    }

    private void updateAllMessageRequest(final View v, final int me_id, final String token_user,
                                      final int friend_id) {

        String url = HelperWS.BASE_URL + "updateallmessage";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                BaseResponseWS responseServer = null;

                responseServer = gson.fromJson(response, BaseResponseWS.class);

                final String errorMsj = responseServer.statusMessage;

                if (responseServer.statusCode == 10 || responseServer.statusCode == 8) {
                    Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();
                } else if (responseServer.statusCode == 0) {

                    ContentValues nuevoRegistro = new ContentValues();
                    nuevoRegistro.put("status_read_msj", 1);

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(getActivity());
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();

                    db.update("at_mess_messages", nuevoRegistro, "fk_usuario_receiver = ? AND fk_usuario_sender = ?", new String[]{String.valueOf(me_id), String.valueOf(friend_id)});

                    db.close();

                } else if (responseServer.statusCode == 4) {

                    User_Information_Helper create_database_user_information =
                            new User_Information_Helper(getActivity().getApplicationContext());
                    SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                    db.delete("at_mess_users", null, null);
                    db.close();

                    Toast.makeText(getActivity().getApplicationContext(), errorMsj, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent();
                    intent.setClass(getActivity().getApplicationContext(), LoginActivity.class);
                    getActivity().finish();
                    startActivity(intent);

                }

                onConnectionFinished();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onConnectionFinished();

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("ValidacionAcceso", token_user);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_receiver", String.valueOf(me_id));
                params.put("id_sender", String.valueOf(friend_id));
                return params;
            }
        };
        addToQueue(postRequest);
    }

}