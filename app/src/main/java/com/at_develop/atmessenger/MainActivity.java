package com.at_develop.atmessenger;

/**
 * Created by jgcapistran on 10/09/17.
 * Developed by: AT Develop
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_VIBRATE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        findViewById(R.id.menu_messages).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.menu_messages:
                                selectedFragment = ChatsFragment.newInstance();
                                findViewById(R.id.menu_messages).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                findViewById(R.id.menu_config).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                findViewById(R.id.menu_contacts).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                break;
                            case R.id.menu_contacts:
                                selectedFragment = ContactsFragment.newInstance();
                                findViewById(R.id.menu_messages).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                findViewById(R.id.menu_config).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                findViewById(R.id.menu_contacts).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                break;
                            case R.id.menu_config:
                                selectedFragment = ConfigFragment.newInstance();
                                findViewById(R.id.menu_messages).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                findViewById(R.id.menu_config).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                findViewById(R.id.menu_contacts).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                break;
                        }

                        FragmentManager fm = getFragmentManager();
                        FragmentTransaction transaction = fm.beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();

                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.frame_layout, new ChatsFragment());
        transaction.commit();
        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);

    }


}
