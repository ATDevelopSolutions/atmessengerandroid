package com.at_develop.atmessenger.WebServices;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jgcapistran on 28/09/17.
 */

public class BaseResponseWS {
    public int statusCode;
    public String statusMessage;
    public String module;
    public String method;
    public HashMap resultData;
    public JsonArray friendsData;
    public JsonArray messagesData;
    public Object request;
    public Object params;
    public HashMap post;
    public Boolean authorization;
    public HashMap session;
    public String api_file;
    public double elapsed_time;
}
