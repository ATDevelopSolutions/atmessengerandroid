package com.at_develop.atmessenger.WebServices;

import java.util.HashMap;

/**
 * Created by jgcapistran on 09/10/17.
 */

public class FriendsResponseWS {
    public int idUser;
    public String username;
    public String email;
    public String firstName;
    public String lastName;
    public String phone;
    public String profileImg;
    public int status;
    public String estadoUser;
    public int statusUser;
    public int sexo;
    public int pais;
    public int statusFriendRequest;
    public int senderUser;
}
