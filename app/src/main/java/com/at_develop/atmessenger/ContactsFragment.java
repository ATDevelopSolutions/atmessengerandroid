package com.at_develop.atmessenger;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.at_develop.atmessenger.Database.Helper.User_Information_Helper;
import com.at_develop.atmessenger.Database.Schema.User_Friends_Schema;
import com.at_develop.atmessenger.FuctionsHelpers.HelperWS;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ChatDataProvider;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ContactsContract;
import com.at_develop.atmessenger.RecyclerViews.Adapters.ContactsDataAdapter;
import com.at_develop.atmessenger.WebServices.BaseResponseWS;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import static com.android.volley.toolbox.Volley.newRequestQueue;

/**
 * Created by jgcapistran on 16/09/17.
 * Developed by: AT Develop
 */

public class ContactsFragment extends BaseVolleyActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    int us_id, status, status_user, sex, pais, phone;
    String first_name, last_name, user_name, email, estado_user, token_user, profile_img;
    private RequestQueue reQueue;
    private ContactsDataAdapter ContactsAdapter;
    ImageView list_new_friend;
    RecyclerView mContactsRecycler;

    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_fragment_contacts, container, false);

        us_id = getActivity().getIntent().getExtras().getInt("us_id");
        status = getActivity().getIntent().getExtras().getInt("us_status");
        status_user = getActivity().getIntent().getExtras().getInt("us_status_user");
        estado_user = getActivity().getIntent().getExtras().getString("us_estado_user");
        first_name = getActivity().getIntent().getExtras().getString("us_first_name");
        last_name = getActivity().getIntent().getExtras().getString("us_last_name");
        email = getActivity().getIntent().getExtras().getString("us_email");
        user_name = getActivity().getIntent().getExtras().getString("us_username");
        profile_img = getActivity().getIntent().getExtras().getString("us_profile_img");
        sex = getActivity().getIntent().getExtras().getInt("us_sex");
        pais = getActivity().getIntent().getExtras().getInt("us_pais");
        phone = getActivity().getIntent().getExtras().getInt("us_cellphone");
        token_user = getActivity().getIntent().getExtras().getString("us_token");

        v.findViewById(R.id.add_user_friend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getActivity().getLayoutInflater();

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                View dialogView = inflater.inflate(R.layout.layout_add_friend, null);
                final EditText username_friend = (EditText) dialogView.findViewById(R.id.username_friend);

                builder.setView(dialogView)
                        // Add action buttons
                        .setPositiveButton(R.string.title_add_friend, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                String user_friend = username_friend.getText().toString();
                                dialog.dismiss();
                                addFriendRequest(getActivity().getApplicationContext(), token_user, user_friend);
                            }

                            private void addFriendRequest(final Context context, final String token_user, final String friend_username) {

                                String url = HelperWS.BASE_URL + "addfriend";

                                reQueue = newRequestQueue(context);

                                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Gson gson = new Gson();
                                        BaseResponseWS responseServer = null;

                                        responseServer = gson.fromJson(response, BaseResponseWS.class);

                                        final String errorMsj = responseServer.statusMessage;
                                        HashMap resultData = responseServer.resultData;

                                        if (responseServer.statusCode == 9) {

                                            Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                                        } else if (responseServer.statusCode == 0) {

                                            Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                                            ContentValues nuevoRegistro = new ContentValues();
                                            HelperWS helper = new HelperWS();

                                            // Creacion del registro mediante un objeto de ContentValues
                                            nuevoRegistro.put("id", helper.parseIntResponse(resultData,"id"));
                                            nuevoRegistro.put("first_name", helper.parseStringResponse(resultData, "first_name"));
                                            nuevoRegistro.put("last_name", helper.parseStringResponse(resultData, "last_name"));
                                            nuevoRegistro.put("username", helper.parseStringResponse(resultData, "username"));
                                            nuevoRegistro.put("email", helper.parseStringResponse(resultData, "email"));
                                            nuevoRegistro.put("profile_img", helper.parseStringResponse(resultData, "profile_img"));
                                            nuevoRegistro.put("status", 0);
                                            nuevoRegistro.put("status_user", helper.parseIntResponse(resultData,"status_user"));
                                            nuevoRegistro.put("estado_user", helper.parseStringResponse(resultData, "estado_user"));
                                            nuevoRegistro.put("sex", helper.parseIntResponse(resultData,"sexo"));
                                            nuevoRegistro.put("pais", helper.parseIntResponse(resultData,"pais"));
                                            nuevoRegistro.put("phone", helper.parseIntResponse(resultData,"phone"));

                                            getActivity().getApplicationContext().getContentResolver().insert(ChatDataProvider.CONTENT_URI_C, nuevoRegistro);

                                        } else if (responseServer.statusCode == 4) {

                                            User_Information_Helper create_database_user_information =
                                                    new User_Information_Helper(context);
                                            SQLiteDatabase db = create_database_user_information.getWritableDatabase();
                                            db.delete("at_mess_users", null, null);
                                            db.delete("at_mess_messages", null, null);
                                            db.delete("at_mess_friends", null, null);
                                            db.close();

                                            Toast.makeText(context, errorMsj, Toast.LENGTH_LONG).show();

                                            Intent intent = new Intent();
                                            intent.setClass(context, LoginActivity.class);
                                            startActivity(intent);

                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
                                    }
                                }) {
                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        HashMap<String, String> headers = new HashMap<>();
                                        headers.put("ValidacionAcceso", token_user);
                                        return headers;
                                    }

                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("user_name", String.valueOf(friend_username));
                                        return params;
                                    }

                                };
                                reQueue.add(request);
                            }
                        })
                        .setNegativeButton(R.string.cancel_btn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();

                dialog.show();
            }
        });

        mContactsRecycler = v.findViewById(R.id.my_recycler_view);
        mContactsRecycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        mContactsRecycler.setLayoutManager(llm);
        ContactsAdapter = new ContactsDataAdapter(getActivity().getApplicationContext(), null, ContactsAdapter.FLAG_AUTO_REQUERY);
        ContactsAdapter.notifyDataSetChanged();
        mContactsRecycler.setAdapter(ContactsAdapter);

        getLoaderManager().initLoader(0, null, this).forceLoad();

        return v;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity().getApplicationContext(), ContactsContract.Contacts.CONTENT_URI, User_Friends_Schema.User_Friends_Database.ALL_FIELDS, null, null, ContactsContract.Contacts.DEFAULT_SORT_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        User_Information_Helper create_database_user_information = User_Information_Helper.getInstance(getActivity());
        SQLiteDatabase db = create_database_user_information.getWritableDatabase();
        Cursor result_query = db.rawQuery("SELECT COUNT(*) FROM at_mess_users where status = ?", new String[]{"0"});

        // Nos aseguramos de que existe al menos un registro
        if (result_query.moveToFirst()) {
            // Obtencion de los datos de usuario desde la base de datos
            int new_friend = result_query.getInt(0);

            if (new_friend > 0) {
                list_new_friend = (ImageView) getView().findViewById(R.id.list_friend_request);
                list_new_friend.setBackground(getResources().getDrawable((R.drawable.group_profile_users_wt_new)));
            }

        }

        db.close();

        if (data != null && data.moveToFirst()) {
            ContactsAdapter.swapCursor(data);
            ContactsAdapter.notifyDataSetChanged();
            mContactsRecycler.smoothScrollToPosition(data.getCount());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        ContactsAdapter.swapCursor(null);
    }


}
