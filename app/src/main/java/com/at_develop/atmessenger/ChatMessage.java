package com.at_develop.atmessenger;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

/**
 * Created by cloudsourceit on 17/10/17.
 */

public class ChatMessage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_chat_message);
        getWindow().setBackgroundDrawableResource(R.drawable.back_2);
    }
}
